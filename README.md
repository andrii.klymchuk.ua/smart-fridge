# Smart fridge

> ### Description
>An application that simulates the work of a "smart fridge",
> which will analyze what products are available in the fridge
> and which
> you need to buy more for cooking our selected recipes.
>

## Tech, Framework and other Dependencies

* Java version: **OpenJDK 11**
* Gradle version: **7.4.1**
* SpringBoot version: **2.7.1**
* Database: **PostgreSQL**, **H2** *(for test)*
* Thymeleaf version: **3.0.4**
* Bootstrap version: **5**
* Other:
  * Docker
  * Spring Security
  * JDBC API
  * FlywayDB
  * Validation
* JUnit 5

## Links:

<details>
<summary>Click to view the links</summary>

- [Deploy the project in AWS](http://smart-fridge.eba-6fkpb8b3.eu-central-1.elasticbeanstalk.com/)

</details>


## Building

<details>
<summary>Instructions for building project</summary>

1. Clone this repository.
2. Building project.

**Gradle:**
```shell
./gradlew :build
```
3. Check that the **[name]-SNAPSHOT.jar** has been created.
4. **Database Server** is required to run an application **without Docker**.
5. Edit **application.yml** for connect to you **Database**.
```
  datasource:
    url: jdbc:postgresql://localhost:5432/smart-fridge
    username: postgres
    password: root
    driver-class-name: org.postgresql.Driver
```

</details>

## Running

<details>
<summary>Instructions for running project</summary>

**Gradle:**
```shell
./gradlew :bootRun
```
**Java:** 
```shell
 java -jar [name]-SNAPSHOT.jar
```
**Docker:**
1. Install [Docker Compose](https://docs.docker.com/compose/install/).
2. Define app environment with a **Dockerfile**. 
3. Define the services in **docker-compose.yml**. 
4. Run docker **compose up**.

```
docker-compose.exe -f [path\]docker-compose.yml up -d --build app
```


</details>

## Login for

In App register or/and **Login** to get access to **create/edit products and
recipes**

## Testing

<details>
<summary>Instructions for testing project</summary>

```shell
./gradlew :test
```

</details>
