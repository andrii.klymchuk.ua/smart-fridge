package com.spdu.review.main.controller;

import com.spdu.review.main.converter.UserConvertor;
import com.spdu.review.main.dto.UserDto;
import com.spdu.review.main.model.User;
import com.spdu.review.main.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {

    private final UserService userService;


    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{name}")
    public UserDto get(@PathVariable String name) {
        User user = userService.get(name);

        return UserConvertor.toDto(user);
    }

    @GetMapping("/{name}")
    public List<UserDto> getAll() {
        List<User> users = userService.getAll();

        return UserConvertor.toDto(users);
    }
}
