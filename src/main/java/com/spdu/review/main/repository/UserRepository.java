package com.spdu.review.main.repository;

import com.spdu.review.main.exception.NotFoundException;
import com.spdu.review.main.mapper.UserMapper;
import com.spdu.review.main.model.User;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepository {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private final UserMapper userMapper;

    public UserRepository(NamedParameterJdbcTemplate namedParameterJdbcTemplate, UserMapper userMapper) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
        this.userMapper = userMapper;
    }

    public User get(String name) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource("name", name);
        String query = "SELECT * FROM user WHERE name =:name";

        try {
            return namedParameterJdbcTemplate.queryForObject(query, parameterSource, userMapper);
        } catch (EmptyResultDataAccessException e) {
            throw new NotFoundException("");
        }
    }

    @Nullable
    public User get(Integer id) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource("id", id);
        String query = "SELECT * FROM user WHERE id =:v";

        try {
            return namedParameterJdbcTemplate.queryForObject(query, parameterSource, userMapper);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public List<User> getAll() {
        String query = "SELECT * FROM user";

        return namedParameterJdbcTemplate.query(query, userMapper);
    }
}
