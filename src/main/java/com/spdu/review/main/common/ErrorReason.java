package com.spdu.review.main.common;

public enum ErrorReason {

    BAD_DESCRIPTION,
    BAD_REQUEST,
    NOT_FOUND
}
