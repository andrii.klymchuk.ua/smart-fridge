package com.spdu.review.main.common;

public class ErrorDto {

    private ErrorReason errorReason;
    private String message;

    public ErrorDto(ErrorReason errorReason, String message) {
        this.errorReason = errorReason;
        this.message = message;
    }

    public ErrorReason getErrorReason() {
        return errorReason;
    }

    public void setErrorReason(ErrorReason errorReason) {
        this.errorReason = errorReason;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
