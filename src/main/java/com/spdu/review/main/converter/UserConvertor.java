package com.spdu.review.main.converter;

import com.spdu.review.main.dto.UserDto;
import com.spdu.review.main.model.User;

import java.util.List;
import java.util.stream.Collectors;

public class UserConvertor {

    private UserConvertor() {
    }

    public static List<UserDto> toDto(List<User> users) {
        return users.stream()
            .map(UserConvertor::toDto)
            .collect(Collectors.toList());
    }

    public static UserDto toDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setName(user.getName());

        return userDto;
    }

    public static User fromDto(UserDto userDto) {
        User user = new User();
        user.setName(userDto.getName());

        return user;
    }
}
