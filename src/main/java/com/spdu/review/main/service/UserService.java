package com.spdu.review.main.service;

import com.spdu.review.main.model.User;
import com.spdu.review.main.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User get(String name) {
        return userRepository.get(name);
    }

    public List<User> getAll() {
        return userRepository.getAll();
    }
}
