package com.spdu.repository;

import com.spdu.mapper.RecipeMapper;
import com.spdu.model.Recipe;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Repository
public class RecipeRepositoryImpl implements EntityRepository<Recipe> {

    private final String SQL_SELECT_ALL = "SELECT id, name FROM recipe ORDER BY id";
    private final String SQL_SELECT = "SELECT id, name FROM recipe WHERE id IN (:ids) ORDER BY id";
    private final String SQL_DELETE = "DELETE FROM recipe WHERE id = :id";
    private final String SQL_INSERT = "INSERT INTO recipe (name) VALUES (:name)";
    private final String SQL_UPDATE = "UPDATE recipe SET name=:name WHERE id=:id";

    private final NamedParameterJdbcTemplate jdbcTemplate;
    public final RecipeMapper recipeMapper;

    public RecipeRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate, RecipeMapper recipeMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.recipeMapper = recipeMapper;
    }

    @Override
    public List<Recipe> get(@NonNull Collection<Integer> ids) {
        final MapSqlParameterSource parameterSource = new MapSqlParameterSource("ids", ids);

        return jdbcTemplate.query(SQL_SELECT, parameterSource, recipeMapper);
    }

    @Override
    public List<Recipe> getAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, recipeMapper);
    }

    @Override
    public void delete(@NonNull Recipe recipe) {
        jdbcTemplate.update(SQL_DELETE, new MapSqlParameterSource("id", recipe.getId()));
    }

    @Override
    public void save(@NonNull Recipe recipe) {
        if (recipe.getId() == null) {
            insert(recipe);
        } else {
            update(recipe);
        }
    }

    private MapSqlParameterSource getSource(Recipe recipe) {
        return new MapSqlParameterSource("id", recipe.getId())
                .addValue("name", recipe.getName());
    }

    private void insert(Recipe recipe) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(SQL_INSERT, getSource(recipe), keyHolder, new String[]{"id"});
        int id = (Integer) Objects.requireNonNull(keyHolder.getKeys()).get("id");
        recipe.setId(id);
    }

    private void update(Recipe recipe) {
        jdbcTemplate.update(SQL_UPDATE, getSource(recipe));
    }
}
