package com.spdu.repository;

import com.spdu.mapper.FridgeProductMapper;
import com.spdu.model.FridgeProduct;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Repository
public class FridgeProductRepositoryImpl implements EntityRepository<FridgeProduct> {

    private final String SQL_SELECT_ALL = "SELECT product_id, amount FROM fridge_product";
    private final String SQL_SELECT = "SELECT product_id, amount FROM fridge_product WHERE product_id IN (:recipeIds)";
    private final String SQL_DELETE = "DELETE FROM fridge_product WHERE product_id = :id";
    private final String SQL_INSERT = "INSERT INTO fridge_product (product_id, amount) VALUES (:product_id, :amount)";
    private final String SQL_UPDATE = "UPDATE fridge_product SET amount=:amount WHERE product_id=:product_id";

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final FridgeProductMapper fridgeProductMapper;

    public FridgeProductRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate, FridgeProductMapper fridgeProductMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.fridgeProductMapper = fridgeProductMapper;
    }

    @Override
    public List<FridgeProduct> get(@NonNull Collection<Integer> productIds) {
        final MapSqlParameterSource parameterSource = new MapSqlParameterSource("recipeIds", productIds);

        return jdbcTemplate.query(SQL_SELECT, parameterSource, fridgeProductMapper);
    }

    @Override
    public List<FridgeProduct> getAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, fridgeProductMapper);
    }

    @Override
    public void delete(@NonNull FridgeProduct product) {
        jdbcTemplate.update(SQL_DELETE, new MapSqlParameterSource("id", product.getProductId()));
    }

    @Override
    public void save(@NonNull FridgeProduct product) {
        List<FridgeProduct> existIngredients = get(List.of(product.getProductId()));
        long productCount = existIngredients.stream()
                .filter(item-> Objects.equals(item.getProductId(), product.getProductId()))
                .count();

        if (productCount == 0) {
            insert(product);
        } else {
            update(product);
        }
    }

    private void insert(FridgeProduct product) {
        jdbcTemplate.update(SQL_INSERT, getSource(product));
    }

    private void update(FridgeProduct product) {
        jdbcTemplate.update(SQL_UPDATE, getSource(product));
    }

    private MapSqlParameterSource getSource(FridgeProduct ingredient) {
        return new MapSqlParameterSource("product_id", ingredient.getProductId())
                .addValue("amount", ingredient.getAmount());
    }
}
