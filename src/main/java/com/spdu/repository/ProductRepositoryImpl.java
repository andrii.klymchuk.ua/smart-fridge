package com.spdu.repository;

import com.spdu.mapper.ProductMapper;
import com.spdu.model.Product;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Repository
public class ProductRepositoryImpl implements EntityRepository<Product> {

    private final String SQL_SELECT_ALL = "SELECT id, name, measure, category, price FROM product";
    private final String SQL_SELECT = "SELECT id, name, measure, category, price FROM product WHERE id IN (:ids)";
    private final String SQL_DELETE = "DELETE FROM product WHERE id = :id";
    private final String SQL_INSERT = "INSERT INTO product (name, measure, category, price) VALUES (:name, :measure, :category, :price)";
    private final String SQL_UPDATE = "UPDATE product SET name=:name, measure=:measure, category=:category, price=:price WHERE id=:id";

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final ProductMapper productMapper;

    public ProductRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate, ProductMapper productMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.productMapper = productMapper;
    }

    @Override
    public List<Product> get(@NonNull Collection<Integer> ids) {
        final MapSqlParameterSource parameterSource = new MapSqlParameterSource("ids", ids);

        return jdbcTemplate.query(SQL_SELECT, parameterSource, productMapper);
    }

    @Override
    public List<Product> getAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, productMapper);
    }

    @Override
    public void delete(@NonNull Product product) {
        jdbcTemplate.update(SQL_DELETE, new MapSqlParameterSource("id", product.getId()));
    }

    @Override
    public void save(@NonNull Product product) {
        if (product.getId() == null) {
            insert(product);
        } else {
            update(product);
        }
    }

    private MapSqlParameterSource getSource(Product product) {
        return new MapSqlParameterSource("id", product.getId())
            .addValue("name", product.getName())
            .addValue("price", product.getPrice())
            .addValue("measure", product.getMeasure().name())
            .addValue("category", product.getCategory().name());
    }

    private void insert(Product product) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(SQL_INSERT, getSource(product), keyHolder, new String[]{"id"});
        int id = (Integer) Objects.requireNonNull(keyHolder.getKeys()).get("id");
        product.setId(id);
    }

    private void update(Product product) {
        jdbcTemplate.update(SQL_UPDATE, getSource(product));
    }
}
