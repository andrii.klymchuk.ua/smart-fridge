package com.spdu.repository;

import com.spdu.mapper.RecipeIngredientMapper;
import com.spdu.model.RecipeIngredient;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Repository
public class RecipeIngredientRepositoryImpl implements EntityRepository<RecipeIngredient> {

    private final String SQL_SELECT_ALL = "SELECT recipe_id, product_id, amount FROM recipe_ingredient";
    private final String SQL_SELECT = "SELECT recipe_id, product_id, amount FROM recipe_ingredient WHERE recipe_id IN (:recipeIds)";
    private final String SQL_DELETE = "DELETE FROM recipe_ingredient WHERE recipe_id = :recipe_id and product_id=:product_id";
    private final String SQL_INSERT = "INSERT INTO recipe_ingredient (recipe_id, product_id, amount) VALUES (:recipe_id, :product_id, :amount)";
    private final String SQL_UPDATE = "UPDATE recipe_ingredient SET amount=:amount WHERE recipe_id=:recipe_id and product_id=:product_id";

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final RecipeIngredientMapper recipeIngredientMapper;

    public RecipeIngredientRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate,
                                          RecipeIngredientMapper recipeIngredientMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.recipeIngredientMapper = recipeIngredientMapper;
    }

    @Override
    public List<RecipeIngredient> get(@NonNull Collection<Integer> recipeIds) {
        final MapSqlParameterSource parameterSource = new MapSqlParameterSource("recipeIds", recipeIds);

        return jdbcTemplate.query(SQL_SELECT, parameterSource, recipeIngredientMapper);
    }

    @Override
    public List<RecipeIngredient> getAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, recipeIngredientMapper);
    }

    @Override
    public void delete(@NonNull RecipeIngredient ingredient) {
        jdbcTemplate.update(SQL_DELETE, getSource(ingredient));
    }

    @Override
    public void save(@NonNull RecipeIngredient ingredient) {
        List<RecipeIngredient> existIngredients = get(List.of(ingredient.getRecipeId()));
        long productCount = existIngredients.stream()
            .filter(item-> Objects.equals(item.getProductId(), ingredient.getProductId()))
            .count();

       jdbcTemplate.update(
            productCount == 0 ? SQL_INSERT : SQL_UPDATE,
            getSource(ingredient));
    }

    private MapSqlParameterSource getSource(RecipeIngredient ingredient) {
        return new MapSqlParameterSource("recipe_id", ingredient.getRecipeId())
            .addValue("product_id", ingredient.getProductId())
            .addValue("amount", ingredient.getAmount());
    }
}
