package com.spdu.repository;

import java.util.Collection;
import java.util.List;

public interface EntityRepository<T> {

    List<T> get(Collection<Integer> productIds);

    List<T> getAll();

    void delete(T product);

    void save(T product);
}
