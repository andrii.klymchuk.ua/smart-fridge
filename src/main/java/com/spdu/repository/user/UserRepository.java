package com.spdu.repository.user;

import com.spdu.model.user.User;

public interface UserRepository {

    User getByUsername(String username);

    void save(User user);
}
