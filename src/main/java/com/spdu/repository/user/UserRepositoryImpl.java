package com.spdu.repository.user;

import com.spdu.mapper.UserMapper;
import com.spdu.model.user.User;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final UserMapper userMapper;

    public UserRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate, UserMapper userMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.userMapper = userMapper;
    }

    @Override
    public User getByUsername(String username) {
        final String sql = "" +
                " SELECT id, username, password, enabled, role FROM users " +
                " WHERE UPPER(username) LIKE UPPER(:username) ";

        MapSqlParameterSource parameterSource = new MapSqlParameterSource("username", username);
        List<User> users = jdbcTemplate.query(sql, parameterSource, userMapper);

        return users.isEmpty() ? null : users.get(0);
    }

    @Override
    public void save(User user) {
        if (user.getId() == null) {
            insert(user);
        } else {
            update(user);
        }
    }

    private MapSqlParameterSource getSource(User user) {
        return new MapSqlParameterSource("id", user.getId())
                .addValue("username", user.getUsername())
                .addValue("password", user.getPassword())
                .addValue("role", user.getRole());
    }

    private void insert(User user) {
        final String sql = "INSERT INTO users (username, password, role) VALUES (:username, :password, :role)";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(sql, getSource(user), keyHolder, new String[]{"id"});
        int id = (Integer) Objects.requireNonNull(keyHolder.getKeys()).get("id");
        user.setId(id);
    }

    private void update(User user) {
        final String sql = "UPDATE users SET username=:username, password=:password, role=:role WHERE id=:id";
        jdbcTemplate.update(sql, getSource(user));
    }
}
