package com.spdu.controller;

import com.spdu.exception.ValidateException;
import com.spdu.form.ProductForm;
import com.spdu.model.Category;
import com.spdu.model.Measure;
import com.spdu.model.Product;
import com.spdu.service.EntityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping("product")
@Api("List of products controller")
public class ProductController {

    private final EntityService<Product> service;

    public ProductController(EntityService<Product> service) {
        this.service = service;
    }

    @GetMapping
    @ApiOperation("Show list of products in page")
    public ModelAndView index() {
        return new ModelAndView("product")
            .addObject("products", service.getAll())
            .addObject("measures", Measure.values())
            .addObject("categories", Category.values());
    }

    @PostMapping("save")
    @ResponseBody
    @ApiOperation("Save the product (response body)")
    public String save(@ModelAttribute("form") @Valid ProductForm form, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new ValidateException(bindingResult.getAllErrors());
        }

        final Product product = getProduct(form);
        service.save(product);

        return String.format("Product %s save!", product.getName());
    }

    @PostMapping("delete")
    @ApiOperation("Delete product in page")
    public ModelAndView delete(@ModelAttribute("form") @Valid ProductForm form, BindingResult bindingResult) {

        final ModelAndView model = new ModelAndView("redirect:/product");
        if (bindingResult.hasErrors() || form.getId() == null) {
            return model.addObject("message", "Product can`t be delete!");
        }

        final Product product = getProduct(form);
        service.delete(product);

        return model.addObject("message", String.format("Product by id:%d delete!", form.getId()));
    }

    private Product getProduct(ProductForm form) {
        final Product product = new Product();
        product.setId(form.getId());
        product.setName(form.getName());
        product.setPrice(form.getPrice());
        product.setMeasure(form.getMeasure());
        product.setCategory(form.getCategory());

        return product;
    }
}
