package com.spdu.controller;

import com.spdu.exception.UserRegistrationException;
import com.spdu.form.RegistrationForm;
import com.spdu.model.user.User;
import com.spdu.service.user.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@Api("Main controller")
public class MainController {

    private final UserService service;

    public MainController(UserService service) {
        this.service = service;
    }

    @GetMapping("/")
    @ApiOperation("Main page")
    public ModelAndView index() {
        return new ModelAndView("index");
    }

    @RequestMapping("login")
    @ApiOperation("Login page")
    public ModelAndView login() {
        return new ModelAndView("signIn");
    }

    @GetMapping("registration")
    @ApiOperation("Registration page")
    public String initRegistration() {
        return "signUp";
    }

    @PostMapping("registration")
    @ApiOperation("Create new user and show result in Registration page")
    public ModelAndView registration(
            @ModelAttribute("form") @Valid RegistrationForm form,
            BindingResult bindingResult
    ) {
        if (bindingResult.hasErrors()) {
            throw new UserRegistrationException(bindingResult.getAllErrors(), "Validate exception");
        }

        if (!form.getPassword().equals(form.getRepeat())) {
            throw new UserRegistrationException("Password and repeat not equals");
        }

        final User user = new User();
        user.setUsername(form.getUsername());
        user.setPassword(new BCryptPasswordEncoder().encode(form.getPassword()));
        user.setEnabled(true);
        user.setRole("USER");
        service.save(user);

        return new ModelAndView("signUp").addObject("message", "User create!");
    }
}
