package com.spdu.controller;

import com.spdu.exception.ValidateException;
import com.spdu.form.RecipeIngredientForm;
import com.spdu.model.RecipeIngredient;
import com.spdu.service.EntityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("ingredient")
@Api("List of recipe ingredients REST controller")
public class RecipeIngredientController {

    private final EntityService<RecipeIngredient> service;

    public RecipeIngredientController(EntityService<RecipeIngredient> service) {
        this.service = service;
    }

    @PostMapping("save")
    @ApiOperation("Save the recipe ingredient (response body)")
    public String save(@ModelAttribute("form") @Valid RecipeIngredientForm form, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            throw new ValidateException(bindingResult.getAllErrors());
        }

        final RecipeIngredient ingredient = getRecipeIngredient(form);
        service.save(ingredient);

        return "Recipe ingredient save!";
    }

    @PostMapping("delete")
    @ApiOperation("Delete recipe ingredient (response body)")
    public String delete(@ModelAttribute("form") @Valid RecipeIngredientForm form, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "Recipe ingredient can`t be delete!";
        }

        final RecipeIngredient ingredient = getRecipeIngredient(form);
        service.delete(ingredient);

        return "Recipe ingredient delete!";
    }

    private RecipeIngredient getRecipeIngredient(RecipeIngredientForm form) {
        final RecipeIngredient ingredient = new RecipeIngredient();
        ingredient.setRecipeId(form.getRecipeId());
        ingredient.setProductId(form.getProductId());
        ingredient.setAmount(form.getAmount());

        return ingredient;
    }
}
