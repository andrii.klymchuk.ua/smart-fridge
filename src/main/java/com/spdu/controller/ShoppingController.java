package com.spdu.controller;

import com.spdu.dto.MapperDto;
import com.spdu.dto.ProductDto;
import com.spdu.exception.ValidateException;
import com.spdu.form.ShoppingForm;
import com.spdu.model.Order;
import com.spdu.model.Recipe;
import com.spdu.service.EntityService;
import com.spdu.service.logic.ShoppingListService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("shopping")
@Api("Shopping controller")
public class ShoppingController {

    private final EntityService<Recipe> recipeService;
    private final ShoppingListService shoppingListService;
    private final MapperDto mapperDto;

    public ShoppingController(EntityService<Recipe> recipeService, ShoppingListService shoppingListService, MapperDto mapperDto) {
        this.recipeService = recipeService;
        this.shoppingListService = shoppingListService;
        this.mapperDto = mapperDto;
    }

    @GetMapping
    @ApiOperation("Show list of recipes in page")
    public ModelAndView index() {
        return new ModelAndView("shopping")
            .addObject("recipes", recipeService.getAll());
    }

    @PostMapping
    @ApiOperation("Show lists of recipes and products for shopping in page")
    public ModelAndView list(@ModelAttribute("form") @Valid ShoppingForm form, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new ValidateException(bindingResult.getAllErrors());
        }

        final List<Order> orders = shoppingListService.getShoppingList(form.getRecipeIds());
        final List<ProductDto> products = shoppingListService.getSortedShoppingList(orders).stream()
            .map(mapperDto::orderToProductDto)
            .collect(Collectors.toList());

        return new ModelAndView("shopping")
            .addObject("recipes", recipeService.getAll())
            .addObject("products", products)
            .addObject("total", shoppingListService.getTotals(orders));
    }
}
