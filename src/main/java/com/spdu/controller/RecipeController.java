package com.spdu.controller;

import com.spdu.dto.MapperDto;
import com.spdu.dto.RecipeDto;
import com.spdu.exception.ValidateException;
import com.spdu.form.RecipeForm;
import com.spdu.model.Product;
import com.spdu.model.Recipe;
import com.spdu.service.EntityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("recipe")
@Api("List of recipes controller")
public class RecipeController {

    private final EntityService<Recipe> recipeService;
    private final EntityService<Product> productService;
    private final MapperDto mapperDto;

    public RecipeController(EntityService<Recipe> recipeService, EntityService<Product> productService, MapperDto mapperDto) {
        this.recipeService = recipeService;
        this.productService = productService;
        this.mapperDto = mapperDto;
    }

    @GetMapping
    @ApiOperation("List of recipes in page")
    public ModelAndView index() {
        return new ModelAndView("recipe")
            .addObject("recipes", recipeService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation("Show list of recipe in page")
    public ModelAndView get(@PathVariable int id) {
        final List<Recipe> recipes = recipeService.getAll();
        final Recipe recipe = recipes.stream()
            .filter(e->e.getId() == id)
            .findFirst().orElse(null);

        final List<Product> products = productService.getAll();
        final RecipeDto recipeDto = mapperDto.mapRecipeToRecipeDto(recipe);

        return new ModelAndView("recipe")
            .addObject("selectRecipe", recipeDto)
            .addObject("recipes", recipes)
            .addObject("products", products);
    }

    @PostMapping("save")
    @ApiOperation("Save recipe (response body)")
    @ResponseBody
    public String save(@ModelAttribute("form") @Valid RecipeForm form, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new ValidateException(bindingResult.getAllErrors());
        }

        final Recipe recipe = getRecipe(form);
        recipeService.save(recipe);

        return String.format("Recipe %s save!", recipe.getName());
    }

    @PostMapping("delete")
    @ApiOperation("Delete recipe")
    public ModelAndView delete(@ModelAttribute("form") @Valid RecipeForm form, BindingResult bindingResult) {
        final ModelAndView model = new ModelAndView("redirect:/recipe");
        if (bindingResult.hasErrors() || form.getId() == null) {
            return model.addObject("message", "Recipe can`t be delete!");
        }

        final Recipe recipe = getRecipe(form);
        recipeService.delete(recipe);

        return model.addObject("message", String.format("Recipe by id:%d delete!", form.getId()));
    }

    private Recipe getRecipe(RecipeForm form) {
        final Recipe recipe = new Recipe();
        recipe.setId(form.getId());
        recipe.setName(form.getName());

        return recipe;
    }
}
