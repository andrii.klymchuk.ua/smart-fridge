package com.spdu.controller;

import com.spdu.dto.MapperDto;
import com.spdu.dto.ProductDto;
import com.spdu.exception.ValidateException;
import com.spdu.form.FridgeProductForm;
import com.spdu.model.FridgeProduct;
import com.spdu.model.Product;
import com.spdu.service.EntityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("fridge")
@Api("Fridge products update controller")
public class FridgeProductController {

    private final EntityService<Product> productService;
    private final EntityService<FridgeProduct> fridgeProductService;
    private final MapperDto mapperDto;

    public FridgeProductController(EntityService<Product>  productService, EntityService<FridgeProduct> fridgeProductService, MapperDto mapperDto) {
        this.productService = productService;
        this.fridgeProductService = fridgeProductService;
        this.mapperDto = mapperDto;
    }

    @GetMapping
    @ApiOperation("Show list of fridge products in page")
    public ModelAndView index() {
        final List<Product> products = productService.getAll();
        final List<FridgeProduct> fridgeProducts = fridgeProductService.getAll();

        final List<ProductDto> fridgeProductsDtos = mapperDto.mapFridgeProductToProductDto(fridgeProducts);

        return new ModelAndView("fridge")
            .addObject("enableProducts", products)
            .addObject("fridgeProducts", fridgeProductsDtos);
    }

    @PostMapping("save")
    @ResponseBody
    @ApiOperation("Save the product in the fridge (response body)")
    public String save(@ModelAttribute("form") @Valid FridgeProductForm form, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            throw new ValidateException(bindingResult.getAllErrors());
        }

        final FridgeProduct product = getProduct(form);
        fridgeProductService.save(product);

        return String.format("Product id:%d save to fridge!", product.getProductId());
    }

    @PostMapping("delete")
    @ResponseBody
    @ApiOperation("Delete product from fridge (response body)")
    public String delete(@ModelAttribute("form") @Valid FridgeProductForm form, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "Product can`t be delete from fridge!";
        }

        final FridgeProduct product = getProduct(form);
        fridgeProductService.delete(product);

        return String.format("Product by id:%d delete from fridge!", product.getProductId());
    }

    private FridgeProduct getProduct(FridgeProductForm form) {
        final FridgeProduct product = new FridgeProduct();
        product.setProductId(form.getProductId());
        product.setAmount(form.getAmount());

        return product;
    }
}
