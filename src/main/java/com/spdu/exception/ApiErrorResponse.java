package com.spdu.exception;

import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.List;

public class ApiErrorResponse {
    private final List<ApiError> errors = new ArrayList<>();

    public ApiErrorResponse() {
    }

    public void addErrors(List<ObjectError> globalErrors) {
        for (ObjectError error : globalErrors) {
            if (error instanceof FieldError) {
                FieldError fieldError = (FieldError) error;
                addError(fieldError);
            } else {
                addError(error);
            }
        }
    }

    public void addError(FieldError fieldError) {
        String field = fieldError.getField();
        if (fieldError.isBindingFailure()) {
            addError(field, "invalid type", ApiError.Type.FIELD);
        } else {
            addError(field, fieldError.getDefaultMessage(), ApiError.Type.FIELD);
        }
    }

    public void addError(ObjectError objectError) {
        addError(objectError.getObjectName(), objectError.getDefaultMessage(), ApiError.Type.GLOBAL);
    }

    public void addError(String error, String message, ApiError.Type type) {
        errors.add(new ApiError(error, message, type));
    }

    public List<ApiError> getErrors() {
        return errors;
    }
}