package com.spdu.exception;

public class ApiError {
    private String error;
    private String message;
    private Type type;

    public enum Type {
        FIELD,
        GLOBAL
    }

    public ApiError(String error, String message, Type type) {
        this.error = error;
        this.message = message;
        this.type = type;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}