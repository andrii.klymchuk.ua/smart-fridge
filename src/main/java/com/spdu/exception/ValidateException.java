package com.spdu.exception;

import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.List;

public class ValidateException extends RuntimeException{
    private final List<ObjectError> errors;

    public ValidateException(List<ObjectError> errors) {
        super("Validate exception");
        this.errors = errors;
    }

    public ValidateException(String message) {
        super(message);
        this.errors = new ArrayList<>();
    }

    public List<ObjectError> getErrors() {
        return errors;
    }
}
