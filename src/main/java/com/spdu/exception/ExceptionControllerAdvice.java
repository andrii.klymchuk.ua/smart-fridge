package com.spdu.exception;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import java.sql.SQLException;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Controller
@ControllerAdvice
public class ExceptionControllerAdvice extends ResponseEntityExceptionHandler implements ErrorController {

    @RequestMapping("/error")
    public ModelAndView handleError(HttpServletRequest request) {
        final Object exception = request.getAttribute(RequestDispatcher.ERROR_EXCEPTION);

        final HttpStatus status;
        final String message;

        if (isNull(exception)) {
            status = NOT_FOUND;
            message = "Page not found";
        } else {
            status = INTERNAL_SERVER_ERROR;
            message = status.getReasonPhrase();
        }

        return new ModelAndView("error")
            .addObject("message", message)
            .addObject("status", status);
    }

    @ExceptionHandler(ValidateException.class)
    protected ResponseEntity<Object> onValidateException(ValidateException ex) {
        final ApiErrorResponse response = new ApiErrorResponse();
        response.addErrors(ex.getErrors());

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UserRegistrationException.class)
    protected ModelAndView onRegistrationException(UserRegistrationException ex) {
        final ModelAndView modelAndView = new ModelAndView("registration");
        String message = ex.getMessage() + "<br />" +
            ex.getErrors().stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.joining("<br />"));
        modelAndView.addObject("error", message);

        return modelAndView;
    }

    @ExceptionHandler(SQLException.class)
    protected ResponseEntity<Object> onSQLException(SQLException ex) {
        final ApiErrorResponse response = new ApiErrorResponse();
        response.addError("SQLException", "Exception work with database", ApiError.Type.GLOBAL);

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public ResponseEntity<Object> handleMaxSizeException(MaxUploadSizeExceededException ex) {
        final ApiErrorResponse response = new ApiErrorResponse();
        response.addError("MaxSizeException", ex.getMessage(), ApiError.Type.FIELD);

        return new ResponseEntity<>(response, HttpStatus.EXPECTATION_FAILED);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException ex) {
        final ApiErrorResponse response = new ApiErrorResponse();
        final Set<ConstraintViolation<?>> violations = ex.getConstraintViolations();
        if (violations.size() < 5) {
            violations.forEach(violation -> response.addError("ConstraintViolationException", violation.getMessage(), ApiError.Type.FIELD));
        } else {
            String errorMessage = String.format("ConstraintViolationException occurred (identification %d exceptions)", violations.size());
            response.addError(errorMessage, ex.getMessage(), ApiError.Type.FIELD);
        }

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @Override
    @NonNull
    protected ResponseEntity<Object> handleBindException(
            BindException ex,
            @NonNull HttpHeaders headers,
            @NonNull HttpStatus status,
            @NonNull WebRequest request
    ) {
        final ApiErrorResponse errorResponse = new ApiErrorResponse();
        errorResponse.addErrors(ex.getBindingResult().getAllErrors());

        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @Override
    @NonNull
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            @NonNull MethodArgumentNotValidException ex,
            @NonNull HttpHeaders headers,
            @NonNull HttpStatus status,
            @NonNull WebRequest request
    ) {
        return handleBindException(ex, headers, status, request);
    }
}