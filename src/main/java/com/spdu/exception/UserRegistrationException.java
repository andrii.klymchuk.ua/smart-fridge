package com.spdu.exception;

import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.List;

public class UserRegistrationException extends RuntimeException{
    private final List<ObjectError> errors;

    public UserRegistrationException(String message) {
        super(message);
        this.errors = new ArrayList<>();
    }

    public UserRegistrationException(List<ObjectError> errors, String massage) {
        super(massage);
        this.errors = errors;
    }

    public List<ObjectError> getErrors() {
        return errors;
    }
}