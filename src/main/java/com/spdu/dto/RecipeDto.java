package com.spdu.dto;

import java.util.List;
import java.util.Objects;

public class RecipeDto {

    private Integer id;
    private String name;
    private List<ProductDto> products;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ProductDto> getProducts() {
        return products;
    }

    public void setProducts(List<ProductDto> products) {
        this.products = products;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RecipeDto)) return false;

        RecipeDto recipeDto = (RecipeDto) o;

        if (!Objects.equals(id, recipeDto.id)) return false;
        if (!Objects.equals(name, recipeDto.name)) return false;
        return Objects.equals(products, recipeDto.products);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (products != null ? products.hashCode() : 0);
        return result;
    }
}
