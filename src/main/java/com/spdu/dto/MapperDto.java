package com.spdu.dto;

import com.spdu.model.*;
import com.spdu.service.logic.OrderService;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

@Component
public class MapperDto {

    private final OrderService orderService;

    public MapperDto(OrderService orderService) {
        this.orderService = orderService;
    }

    public List<ProductDto> mapFridgeProductToProductDto(List<FridgeProduct> fridgeProducts) {
        if (fridgeProducts == null || fridgeProducts.isEmpty()) {
            return Collections.emptyList();
        }

        final Map<Integer, Double> amountsMap = getAmountsFromFridgeProducts(fridgeProducts);

        return orderService.get(amountsMap).stream()
            .map(this::orderToProductDto)
            .collect(Collectors.toList());
    }

    public RecipeDto mapRecipeToRecipeDto(Recipe recipe) {
        if (recipe == null) {
            return null;
        }

        final RecipeDto recipeDto = recipeToRecipeDto(recipe);
        if (recipe.getRecipeIngredients().isEmpty()) {
            return recipeDto;
        }

        final Map<Integer, Double> amountsMap = getAmountsFromRecipeIngredients(recipe.getRecipeIngredients());
        final List<ProductDto> productDtos = orderService.get(amountsMap).stream()
            .map(this::orderToProductDto)
            .collect(Collectors.toList());

        recipeDto.setProducts(productDtos);

        return recipeDto;
    }

    public ProductDto orderToProductDto(Order order){
        if (order == null) {
            return null;
        }

        ProductDto productDto = new ProductDto();
        productDto.setId(order.getProduct().getId());
        productDto.setName(order.getProduct().getName());
        productDto.setPrice(order.getProduct().getPrice());
        productDto.setMeasure(order.getProduct().getMeasure());
        productDto.setCategory(order.getProduct().getCategory());
        productDto.setAmount(order.getAmount());

        return productDto;
    }

    private RecipeDto recipeToRecipeDto(Recipe recipe){
        final RecipeDto recipeDto = new RecipeDto();
        recipeDto.setId(recipe.getId());
        recipeDto.setName(recipe.getName());

        return recipeDto;
    }

    private Map<Integer, Double> getAmountsFromRecipeIngredients(List<RecipeIngredient> recipeIngredients) {
        return recipeIngredients.stream()
                .collect(toMap(RecipeIngredient::getProductId, RecipeIngredient::getAmount));
    }

    private Map<Integer, Double> getAmountsFromFridgeProducts(List<FridgeProduct> fridgeProducts) {
        return fridgeProducts.stream()
                .collect(toMap(FridgeProduct::getProductId, FridgeProduct::getAmount));
    }
}
