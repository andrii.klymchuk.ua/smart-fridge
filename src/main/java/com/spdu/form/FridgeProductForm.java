package com.spdu.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class FridgeProductForm {

    @NotNull
    @Positive
    private Integer productId;
    @NotNull
    @Positive
    private double amount;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
