package com.spdu.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class RecipeIngredientForm {

    @NotNull
    @Positive
    private Integer recipeId;
    @NotNull
    @Positive
    private Integer productId;
    @NotNull
    @Positive
    private double amount;

    public Integer getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(Integer recipeId) {
        this.recipeId = recipeId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
