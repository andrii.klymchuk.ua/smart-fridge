package com.spdu.form;

import javax.validation.constraints.Pattern;

public class RecipeForm {

    private Integer id;
    @Pattern(
            regexp = "^[а-яА-ЯїЇa-zA-Z0-9 ]{1,255}$",
            message = "Name can`t be empty and mast contains symbols of the alphabet and digits."
    )
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
