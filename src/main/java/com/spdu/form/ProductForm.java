package com.spdu.form;

import com.spdu.model.Category;
import com.spdu.model.Measure;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

public class ProductForm {

    private Integer id;
    @Pattern(
            regexp = "^[а-яА-ЯїЇa-zA-Z0-9 ]{1,255}$",
            message = "Name can`t be empty and mast contains symbols of the alphabet and digits."
    )
    private String name;
    @NotNull
    @Positive
    private BigDecimal price;
    @NotNull
    private Measure measure;
    @NotNull
    private Category category;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Measure getMeasure() {
        return measure;
    }

    public void setMeasure(Measure measure) {
        this.measure = measure;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
