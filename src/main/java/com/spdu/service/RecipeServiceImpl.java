package com.spdu.service;

import com.spdu.model.Recipe;
import com.spdu.model.RecipeIngredient;
import com.spdu.repository.EntityRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class RecipeServiceImpl implements EntityService<Recipe> {

    private final EntityRepository<Recipe> recipeRepository;
    private final EntityService<RecipeIngredient> ingredientService;

    public RecipeServiceImpl(EntityRepository<Recipe> recipeRepository, EntityService<RecipeIngredient> ingredientService) {
        this.recipeRepository = recipeRepository;
        this.ingredientService = ingredientService;
    }

    @Override
    public List<Recipe> get(@NonNull Collection<Integer> ids) {
        List<Recipe> recipes = recipeRepository.get(ids);
        List<RecipeIngredient> ingredients = ingredientService.get(ids);
        setRecipeIngredients(recipes, ingredients);

        return recipes;
    }

    @Override
    public List<Recipe> getAll() {
        List<Recipe> recipes = recipeRepository.getAll();
        List<RecipeIngredient> ingredients = ingredientService.getAll();
        setRecipeIngredients(recipes, ingredients);

        return recipes;
    }

    private void setRecipeIngredients(List<Recipe> recipes, List<RecipeIngredient> ingredients) {
        recipes.forEach(
            recipe -> recipe.setRecipeIngredients(
                ingredients.stream()
                    .filter(ingredient -> Objects.equals(ingredient.getRecipeId(), recipe.getId()))
                    .collect(Collectors.toList()))
        );
    }

    @Override
    public void delete(@NonNull Recipe recipe) {
        recipeRepository.delete(recipe);
    }

    @Override
    public void save(@NonNull Recipe recipe) {
        recipeRepository.save(recipe);
    }
}
