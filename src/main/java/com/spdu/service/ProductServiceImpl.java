package com.spdu.service;

import com.spdu.model.Product;
import com.spdu.repository.EntityRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class ProductServiceImpl implements EntityService<Product> {

    private final EntityRepository<Product> productRepository;

    public ProductServiceImpl(EntityRepository<Product> productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> get(Collection<Integer> ids) {
        return productRepository.get(ids);
    }

    @Override
    public List<Product> getAll() {
        return productRepository.getAll();
    }

    @Override
    public void delete(@NonNull Product product) {
        productRepository.delete(product);
    }

    @Override
    public void save(@NonNull Product product) {
        productRepository.save(product);
    }
}
