package com.spdu.service;

import java.util.Collection;
import java.util.List;

public interface EntityService<T>{

    List<T> get(Collection<Integer> ids);

    List<T> getAll();

    void delete(T recipe);

    void save(T recipe);
}
