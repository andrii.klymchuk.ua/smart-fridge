package com.spdu.service.user;

import com.spdu.model.user.CurrentUser;
import com.spdu.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CurrentUserDetailsService implements UserDetailsService {

    private final UserService service;

    public CurrentUserDetailsService(UserService service) {
        this.service = service;
    }

    @Override
    public CurrentUser loadUserByUsername(String username) throws UsernameNotFoundException {
        final User user = service.loadUserByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(String.format("User by name %s not found", username));
        }
        if (!user.isEnabled()) {
            throw new UsernameNotFoundException(String.format("User by name %s is lock", username));
        }

        return new CurrentUser(user);
    }
}
