package com.spdu.service.user;

import com.spdu.model.user.User;

public interface UserService {

    User loadUserByUsername(String username);

    void save(User user);
}
