package com.spdu.service.user;

import com.spdu.exception.UserRegistrationException;
import com.spdu.model.user.User;
import com.spdu.repository.user.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository repository;

    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public User loadUserByUsername(String username) {
        return repository.getByUsername(username);
    }

    @Override
    public void save(User user) {
        if (user.getId() == null) {
            final User existUser = loadUserByUsername(user.getUsername());
            if (existUser != null) {
                throw new UserRegistrationException("User already exists");
            }
        }

        repository.save(user);
    }
}
