package com.spdu.service;

import com.spdu.model.FridgeProduct;
import com.spdu.repository.EntityRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class FridgeProductServiceImpl implements EntityService<FridgeProduct> {

    private final EntityRepository<FridgeProduct> fridgeProductRepository;

    public FridgeProductServiceImpl(EntityRepository<FridgeProduct> fridgeProductRepository) {
        this.fridgeProductRepository = fridgeProductRepository;
    }

    @Override
    public List<FridgeProduct> get(Collection<Integer> productIds) {
        return fridgeProductRepository.get(productIds);
    }

    @Override
    public List<FridgeProduct> getAll() {
        return fridgeProductRepository.getAll();
    }

    @Override
    public void delete(FridgeProduct product) {
        if (product == null){
            return;
        }

        fridgeProductRepository.delete(product);
    }

    @Override
    public void save(FridgeProduct product) {
        if (product == null){
            return;
        }

        fridgeProductRepository.save(product);
    }
}
