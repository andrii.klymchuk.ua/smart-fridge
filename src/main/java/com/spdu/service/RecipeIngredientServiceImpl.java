package com.spdu.service;

import com.spdu.model.RecipeIngredient;
import com.spdu.repository.EntityRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class RecipeIngredientServiceImpl implements EntityService<RecipeIngredient> {

    private final EntityRepository<RecipeIngredient> repository;

    public RecipeIngredientServiceImpl(EntityRepository<RecipeIngredient> repository) {
        this.repository = repository;
    }

    @Override
    public List<RecipeIngredient> get(@NonNull Collection<Integer> recipeIds) {
        return repository.get(recipeIds);
    }

    @Override
    public List<RecipeIngredient> getAll() {
        return repository.getAll();
    }

    @Override
    public void delete(@NonNull RecipeIngredient ingredient) {
        repository.delete(ingredient);
    }

    @Override
    public void save(@NonNull RecipeIngredient ingredient) {
        repository.save(ingredient);
    }
}
