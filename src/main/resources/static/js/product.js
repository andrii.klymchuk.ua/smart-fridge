let table = '';
$(document).ready(function () {
    table = $('#productTable').DataTable(
        // {"pageLength": 15 }
    );
});

let   filterModal = document.getElementById('saveProductModal');
let   needUpdate  = false;

filterModal.addEventListener('show.bs.modal', function (event) {
    const $saveUrl    = '/product/save';

    const $errors     = $('#errorsModal');
    const $message    = $('#messageModal');
    const $saveBtn    = $('#saveBtn');
    const $dataForm   = $('#formSave');

    const title       = filterModal.querySelector('.modal-title');
    const button      = event.relatedTarget;
    const recipient   = button.getAttribute('data-bs-whatever').split('|');

    setDataToModal(recipient);

    function setDataToModal(data) {
        let $id       = $('#idSave');
        let $name     = $('#nameSave');
        let $price    = $('#priceSave');
        let $measure  = $('#measureSave');
        let $category = $('#categorySave');

        title.textContent = data[0].length
            ? 'Save product by id:' + data[0]
            : 'Save new product';

        $id.val(data[0]);
        $name.val(data[1]);
        $price.val(data[2]);
        $measure.val(data[3]);
        $category.val(data[4]);

        $message.hide();
        $errors.hide();
    }

    function save(data) {
        $.ajax({
            url : $saveUrl,
            type: 'POST',
            data: data,
            beforeSend: function () {
                $message.hide();
                $errors.hide();
                $saveBtn.attr('disabled', true);
            },
            complete: function () {
                $saveBtn.attr('disabled', false);
            }
        }).done(function (data) {
            $message.html(data);
            $message.show();
            needUpdate = true;
        }).fail(function (errors) {
            showError($.parseJSON(errors.responseText).errors, $errors);
        });
    }

    $saveBtn.on('click', function (event) {
        event.preventDefault();
        let data = $dataForm.serialize();
        save(data);
    });

})

filterModal.addEventListener('hide.bs.modal', function () {
    if (needUpdate) {
        location.replace(location.pathname);
    }
})

function showError(data, out) {
    if (data.length) {
        let items = '';
        $.each(data, function (i, item) {
            items += '<p><b>' + item["error"] + '</b>: ' + item["message"] + '</p>';
        });

        out.html(items);
        out.show();
    }
}