let filterFridgeProductModal = document.getElementById('saveFridgeProductModal');
let needFridgeProductUpdate  = false;

filterFridgeProductModal.addEventListener('show.bs.modal', function (event) {
    const $saveUrl    = '/fridge/save';
    const $delUrl     = '/fridge/delete';

    const $delBtn     = $('#delFridgeProductBtn');
    const $errors     = $('#errorsFridgeProductModal');
    const $message    = $('#messageFridgeProductModal');
    const $saveBtn    = $('#saveFridgeProductBtn');

    const $dataForm   = $('#formFridgeProductSave');
    let $newId        = $('#newProductIdSave');
    let $id           = $('#productIdSave');

    const title       = filterFridgeProductModal.querySelector('.modal-title');
    const button      = event.relatedTarget;
    const recipient   = button.getAttribute('data-bs-whatever').split('|');

    setDataToModal(recipient);

    function setDataToModal(data) {
        console.log(data);
        let $name     = $('#nameFridgeSave');
        let $price    = $('#priceFridgeSave');
        let $measure  = $('#measureFridgeSave');
        let $category = $('#categoryFridgeSave');
        let $amount   = $('#amountFridgeProductSave');

        let $newDiv   = $('#newFridgeSave');
        let $editDiv  = $('#editFridgeSave');

        if (data[0].length){
            title.textContent     = 'Save fridge product by id:' + data[0];
            $name.html(data[1]);
            $price.html(data[2]);
            $measure.html(data[3]);
            $category.html(data[4]);

            $id.val(data[0]);

            $newDiv.hide();
            $delBtn.show();
            $editDiv.show();
        } else {
            title.textContent = 'Save new fridge product';

            $newDiv.show();
            $delBtn.hide();
            $editDiv.hide();
        }

        $amount.val(data[5]);

        $errors.hide();
        $message.hide();
    }

    function update(url) {
        let data = $dataForm.serialize();
        $.ajax({
            url : url,
            type: 'POST',
            data: data,
            beforeSend: function () {
                $errors.hide();
                $message.hide();
                $saveBtn.attr('disabled', true);
            },
            complete: function () {
                $saveBtn.attr('disabled', false);
            }
        }).done(function (data) {
            $message.html(data);
            $message.show();
            needFridgeProductUpdate = true;
        }).fail(function (errors) {
            showError($.parseJSON(errors.responseText).errors, $errors);
        });
    }

    $saveBtn.on('click', function (event) {
        event.preventDefault();
        if (!recipient[0].length){
            $id.val($newId.val());
        }

        update($saveUrl);
    });

    $delBtn.on('click', function (event) {
        event.preventDefault();
        update($delUrl);
    });

})

filterFridgeProductModal.addEventListener('hide.bs.modal', function () {
    if (needFridgeProductUpdate) {
        location.replace(location.pathname);
    }
})

function showError(errors, out) {
    if (errors.length) {
        let items = '';
        $.each(errors, function (i, item) {
            items += '<p><b>' + item["error"] + '</b>: ' + item["message"] + '</p>';
        });

        out.html(items);
        out.show();
    }
}