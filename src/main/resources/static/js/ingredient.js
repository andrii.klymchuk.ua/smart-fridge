let filterIngredientModal = document.getElementById('saveIngredientModal');
let needIngredientUpdate  = false;

filterIngredientModal.addEventListener('show.bs.modal', function (event) {
    const $saveUrl    = '/ingredient/save';
    const $delUrl     = '/ingredient/delete';

    const $errors     = $('#errorsIngredientModal');
    const $delBtn     = $('#delIngredientBtn');
    const $saveBtn    = $('#saveIngredientBtn');
    const $message    = $('#messageIngredientModal');
    const $dataForm   = $('#formIngredientSave');

    const title       = filterIngredientModal.querySelector('.modal-title');
    const button      = event.relatedTarget;
    const recipient   = button.getAttribute('data-bs-whatever').split('|');

    setDataToModal(recipient);

    function setDataToModal(data) {
        let $recipe  = $('#recipeIngredientId');
        let $product = $('#productIngredientId');
        let $amount  = $('#amountIngredientSave');

        if (data[0].length > 1){
            title.textContent = 'Save ingredient by id:' + data[1] + ' for recipe:' + data[0];
        } else {
            title.textContent = 'Save new ingredient for recipe:' + data[0];
        }

        $recipe.val(data[0]);
        $amount.val(data[3]);
        $product.val(data[1]);

        $errors.hide();
        $message.hide();
    }

    function update(url) {
        let data = $dataForm.serialize();
        $.ajax({
            url : url,
            type: 'POST',
            data: data,
            beforeSend: function () {
                $errors.hide();
                $message.hide();
                $saveBtn.attr('disabled', true);
            },
            complete: function () {
                $saveBtn.attr('disabled', false);
            }
        }).done(function (data) {
            $message.html(data);
            $message.show();
            needIngredientUpdate = true;
        }).fail(function (errors) {
            showError($.parseJSON(errors.responseText).errors, $errors);
        });
    }

    $saveBtn.on('click', function (event) {
        event.preventDefault();
        update($saveUrl);
    });

    $delBtn.on('click', function (event) {
        event.preventDefault();
        update($delUrl);
    });

})

filterIngredientModal.addEventListener('hide.bs.modal', function () {
    if (needIngredientUpdate) {
        location.replace(location.pathname);
    }
})

function showError(errors, out) {
    if (errors.length) {
        let items = '';
        $.each(errors, function (i, item) {
            items += '<p><b>' + item["error"] + '</b>: ' + item["message"] + '</p>';
        });

        out.html(items);
        out.show();
    }
}