let   filterRecipeModal = document.getElementById('saveRecipeModal');
let   needRecipeUpdate  = false;

filterRecipeModal.addEventListener('show.bs.modal', function (event) {
    const $saveUrl    = '/recipe/save';

    const $errors     = $('#errorsRecipeModal');
    const $message    = $('#messageRecipeModal');
    const $saveBtn    = $('#saveRecipeBtn');
    const $dataForm   = $('#formRecipeSave');

    const title       = filterRecipeModal.querySelector('.modal-title');
    const button      = event.relatedTarget;
    const recipient   = button.getAttribute('data-bs-whatever').split('|');

    setDataToModal(recipient);

    function setDataToModal(data) {
        let $id       = $('#idRecipeSave');
        let $name     = $('#nameRecipeSave');

        title.textContent = data[0].length
            ? 'Save recipe by id:' + data[0]
            : 'Save new recipe';

        $id.val(data[0]);
        $name.val(data[1]);

        $message.hide();
        $errors.hide();
    }

    function save(data) {
        $.ajax({
            url : $saveUrl,
            type: 'POST',
            data: data,
            beforeSend: function () {
                $message.hide();
                $errors.hide();
                $saveBtn.attr('disabled', true);
            },
            complete: function () {
                $saveBtn.attr('disabled', false);
            }
        }).done(function (data) {
            $message.html(data);
            $message.show();
            needRecipeUpdate = true;
        }).fail(function (errors) {
            showError($.parseJSON(errors.responseText).errors, $errors);
        });
    }

    $saveBtn.on('click', function (event) {
        event.preventDefault();
        let data = $dataForm.serialize();
        save(data);
    });

})

filterRecipeModal.addEventListener('hide.bs.modal', function () {
    if (needRecipeUpdate) {
        location.replace(location.pathname);
    }
})

function showError(data, out) {
    if (data.length) {
        let items = '';
        $.each(data, function (i, item) {
            items += '<p><b>' + item["error"] + '</b>: ' + item["message"] + '</p>';
        });

        out.html(items);
        out.show();
    }
}