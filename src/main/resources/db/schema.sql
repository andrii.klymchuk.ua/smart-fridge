CREATE TABLE product
(
    id       SERIAL       NOT NULL,
    name     VARCHAR(255) NOT NULL,
    measure  VARCHAR(255) NOT NULL,
    category VARCHAR(255) NOT NULL,
    price    NUMERIC      NOT NULL,

    PRIMARY KEY (id)
);

CREATE TABLE recipe
(
    id   SERIAL       NOT NULL,
    name VARCHAR(255) NOT NULL,

    PRIMARY KEY (id)
);

CREATE TABLE recipe_ingredient
(
    recipe_id  INTEGER          NOT NULL REFERENCES recipe  (id) ON DELETE CASCADE,
    product_id INTEGER          NOT NULL REFERENCES product (id) ON DELETE CASCADE,
    amount     DOUBLE PRECISION NOT NULL,

    PRIMARY KEY (recipe_id, product_id)
);

CREATE TABLE fridge_product
(
    product_id INTEGER          NOT NULL REFERENCES product (id) ON DELETE CASCADE,
    amount     DOUBLE PRECISION NOT NULL,

    PRIMARY KEY (product_id)
);
