CREATE TABLE recipe
(
    id   SERIAL       NOT NULL,
    name VARCHAR(255) NOT NULL,

    PRIMARY KEY (id)
);
