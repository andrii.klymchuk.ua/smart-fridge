CREATE TABLE recipe_ingredient
(
    recipe_id  INTEGER          NOT NULL REFERENCES recipe  (id) ON DELETE CASCADE,
    product_id INTEGER          NOT NULL REFERENCES product (id) ON DELETE CASCADE,
    amount     DOUBLE PRECISION NOT NULL,

    PRIMARY KEY (recipe_id, product_id)
);