-- Піцца Везувій
INSERT INTO recipe_ingredient (recipe_id, product_id, amount) VALUES (1, 1, 0.5);
INSERT INTO recipe_ingredient (recipe_id, product_id, amount) VALUES (1, 2, 0.5);
INSERT INTO recipe_ingredient (recipe_id, product_id, amount) VALUES (1, 3, 0.1);
INSERT INTO recipe_ingredient (recipe_id, product_id, amount) VALUES (1, 4, 0.2);
INSERT INTO recipe_ingredient (recipe_id, product_id, amount) VALUES (1, 5, 0.1);
INSERT INTO recipe_ingredient (recipe_id, product_id, amount) VALUES (1, 6, 0.15);

-- Салат Цезар
INSERT INTO recipe_ingredient (recipe_id, product_id, amount) VALUES (2, 7, 1);
INSERT INTO recipe_ingredient (recipe_id, product_id, amount) VALUES (2, 2, 0.5);
INSERT INTO recipe_ingredient (recipe_id, product_id, amount) VALUES (2, 8, 0.1);
INSERT INTO recipe_ingredient (recipe_id, product_id, amount) VALUES (2, 9, 0.1);
INSERT INTO recipe_ingredient (recipe_id, product_id, amount) VALUES (2, 10, 0.05);

-- Млинці з творогом і згущеним молоком
INSERT INTO recipe_ingredient (recipe_id, product_id, amount) VALUES (3, 11, 1);
INSERT INTO recipe_ingredient (recipe_id, product_id, amount) VALUES (3, 2, 1);
INSERT INTO recipe_ingredient (recipe_id, product_id, amount) VALUES (3, 1, 1);
INSERT INTO recipe_ingredient (recipe_id, product_id, amount) VALUES (3, 12, 1);
INSERT INTO recipe_ingredient (recipe_id, product_id, amount) VALUES (3, 13, 0.2);
INSERT INTO recipe_ingredient (recipe_id, product_id, amount) VALUES (3, 15, 0.05);

-- Узвар з яблук
INSERT INTO recipe_ingredient (recipe_id, product_id, amount) VALUES (4, 13, 0.3);
INSERT INTO recipe_ingredient (recipe_id, product_id, amount) VALUES (4, 15, 0.5);