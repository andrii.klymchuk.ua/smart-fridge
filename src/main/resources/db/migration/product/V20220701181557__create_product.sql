CREATE TABLE product
(
    id       SERIAL       NOT NULL,
    name     VARCHAR(255) NOT NULL,
    measure  VARCHAR(255) NOT NULL,
    category VARCHAR(255) NOT NULL,
    price    NUMERIC      NOT NULL,

    PRIMARY KEY (id)
);