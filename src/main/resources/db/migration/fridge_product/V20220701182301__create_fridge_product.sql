CREATE TABLE fridge_product
(
    product_id INTEGER          NOT NULL REFERENCES product (id) ON DELETE CASCADE,
    amount     DOUBLE PRECISION NOT NULL,

    PRIMARY KEY (product_id)
);