package com.spdu.dto;

import com.spdu.model.*;
import com.spdu.service.EntityService;
import com.spdu.service.logic.OrderService;
import com.spdu.service.logic.OrderServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class MapperDtoTest {

    @Mock
    private EntityService<Product> productService;

    private static final int PRODUCT_ID = 1;
    private MapperDto mapperDto;

    @BeforeEach
    void setUp() {
        OrderService orderService = new OrderServiceImpl(productService);
        mapperDto = new MapperDto(orderService);
    }

    @Test
    void mapFridgeProductToProductDto() {
        when(productService.get(Set.of(PRODUCT_ID))).thenReturn(List.of(createProduct()));

        List<ProductDto> expectedResult = List.of(createProductDto());
        List<ProductDto> actualResult = mapperDto.mapFridgeProductToProductDto(List.of(createFridgeProduct()));

        assertEquals(expectedResult, actualResult);
        assertEquals(Collections.emptyList(), mapperDto.mapFridgeProductToProductDto(null));
        assertEquals(Collections.emptyList(), mapperDto.mapFridgeProductToProductDto(Collections.emptyList()));
    }

    @Test
    void mapRecipeToRecipeDto() {
        when(productService.get(Set.of(PRODUCT_ID))).thenReturn(List.of(createProduct()));

        RecipeDto expectedResult = createRecipeDto();
        RecipeDto actualResult = mapperDto.mapRecipeToRecipeDto(createRecipe());

        assertEquals(expectedResult, actualResult);
        assertNull(mapperDto.mapRecipeToRecipeDto(null));
    }

    @Test
    void orderToProductDto() {
        ProductDto expectedResult = createProductDto();
        ProductDto actualResult = mapperDto.orderToProductDto(createOrder());

        assertEquals(expectedResult, actualResult);
        assertNull(mapperDto.mapRecipeToRecipeDto(null));
    }

    private Product createProduct() {
        Product product = new Product();
        product.setId(1);
        product.setName("test");
        product.setPrice(BigDecimal.valueOf(50.0));
        product.setCategory(Category.GROCERY);
        product.setMeasure(Measure.KILOGRAM);

        return product;
    }

    private Order createOrder() {
        return new Order(createProduct(), 10);
    }

    private ProductDto createProductDto() {
        ProductDto product = new ProductDto();
        product.setId(1);
        product.setName("test");
        product.setPrice(BigDecimal.valueOf(50.0));
        product.setCategory(Category.GROCERY);
        product.setMeasure(Measure.KILOGRAM);
        product.setAmount(10);

        return product;
    }

    private Recipe createRecipe() {
        Recipe recipe = new Recipe();
        recipe.setId(1);
        recipe.setName("test");
        recipe.setRecipeIngredients(List.of(createRecipeIngredient()));

        return recipe;
    }

    private RecipeDto createRecipeDto() {
        RecipeDto recipeDto = new RecipeDto();
        recipeDto.setId(1);
        recipeDto.setName("test");
        recipeDto.setProducts(List.of(createProductDto()));

        return recipeDto;
    }

    private RecipeIngredient createRecipeIngredient() {
        RecipeIngredient ingredient = new RecipeIngredient();
        ingredient.setProductId(1);
        ingredient.setRecipeId(1);
        ingredient.setAmount(10);

        return ingredient;
    }

    private FridgeProduct createFridgeProduct() {
        FridgeProduct fridgeProduct = new FridgeProduct();
        fridgeProduct.setProductId(1);
        fridgeProduct.setAmount(10);

        return fridgeProduct;
    }
}