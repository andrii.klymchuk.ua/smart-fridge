package com.spdu.controller;

import com.spdu.model.user.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles("test")
class MainControllerTest extends IntegrationTest {

    @Test
    public void givenMainController_ShouldBeNotNull() {
        assertNotNull(context);
        assertNotNull(context.getBean("mainController"));
    }

    @Test
    void givenHomePageURI_ShouldBeTrue() throws Exception {
        SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationToken("test", new BCryptPasswordEncoder().encode("test")));

        final MvcResult mvcResult = mvc.perform(get("/"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("index"))
                .andReturn();

        final String html = mvcResult.getResponse().getContentAsString();

        assertTrue(html.contains("Fridge"));
        assertTrue(html.contains("Shopping list"));
        assertTrue(html.contains("Products"));
        assertTrue(html.contains("Recipes"));
    }

    @Test
    void givenLoginPageURI_ShouldBeTrue() throws Exception {
        final MvcResult mvcResult = mvc.perform(get("/login"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("signIn"))
                .andReturn();

        final String html = mvcResult.getResponse().getContentAsString();

        assertTrue(html.contains("Please enter your login and password!"));
    }

    @Test
    void givenRegistrationPageURI_ShouldBeTrue() throws Exception {
        final MvcResult mvcResult = mvc.perform(get("/registration"))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(view().name("signUp"))
            .andReturn();

        final String html = mvcResult.getResponse().getContentAsString();

        assertTrue(html.contains("Please enter for registration!"));
    }

    @Test
    void chekRegistrationAndLoginStatus_ShouldBeTrue() throws Exception {
        final User user = new User();
        user.setUsername("test");
        user.setPassword("password");

        final MultiValueMap<String, String> form = new LinkedMultiValueMap<>();
        form.put("username", Collections.singletonList(user.getUsername()));
        form.put("password", Collections.singletonList(user.getPassword()));
        form.put("repeat", Collections.singletonList(user.getPassword()));

        mvc.perform(post("/registration")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .params(form))
            .andExpect(status().isOk());

        final MvcResult mvcResult = mvc.perform(post("/login")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .params(form))
            .andExpect(status().isOk())
            .andReturn();

        assertEquals(200, mvcResult.getResponse().getStatus());
    }
}