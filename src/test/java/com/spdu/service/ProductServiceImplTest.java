package com.spdu.service;

import com.spdu.model.Category;
import com.spdu.model.Measure;
import com.spdu.model.Product;
import com.spdu.repository.EntityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProductServiceImplTest {

    private static final int PRODUCT_ID = 1;

    @Mock
    private EntityRepository<Product> productRepository;

    private  EntityService<Product> productService;

    @BeforeEach
    void setUp() {
        productService = new ProductServiceImpl(productRepository);
    }

    @Test
    void getAllProductsByIdWhenProductsExistShouldReturnActualResult() {
        List<Product> expectedResult = List.of(createProduct());

        when(productRepository.get(Set.of(PRODUCT_ID))).thenReturn(List.of(createProduct()));

        List<Product> actualResult = productService.get(Set.of(PRODUCT_ID));

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void getAllProductsWhenProductsExistShouldReturnActualResult() {
        List<Product> expectedResult = List.of(createProduct());

        when(productRepository.getAll()).thenReturn(List.of(createProduct()));

        List<Product> actualResult = productService.getAll();

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void getAllProductsByIdWhenProductsNotExistShouldReturnEmptyList() {
        List<Product> expectedResult = emptyList();

        when(productRepository.get(Set.of(PRODUCT_ID))).thenReturn(emptyList());

        List<Product> actualResult = productService.get(Set.of(PRODUCT_ID));

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void getAllProductsWhenProductsNotExistShouldReturnEmptyList() {
        List<Product> expectedResult = emptyList();

        when(productRepository.getAll()).thenReturn(emptyList());

        List<Product> actualResult = productService.getAll();

        assertEquals(expectedResult, actualResult);
    }


    @Test
    void saveProductMustBeAssert() {
        EntityService<Product> service = mock(ProductServiceImpl.class);
        doNothing().when(service).save(isA(Product.class));

        Product product = new Product();
        service.save(product);
        verify(service).save(product);
        System.out.println(product.getId());

        assertAll(() -> productService.save(product));
        assertAll(() -> productService.save(createProduct()));
    }

    @Test
    void deleteProductMustBeAssert() {
        EntityService<Product> service = mock(ProductServiceImpl.class);
        doNothing().when(service).delete(isA(Product.class));

        Product product = new Product();
        service.delete(product);
        verify(service).delete(product);
        System.out.println(product.getId());

        assertAll(() -> productService.delete(product));
        assertAll(() -> productService.delete(createProduct()));
    }

    private Product createProduct() {
        Product product = new Product();
        product.setId(1);
        product.setName("test");
        product.setPrice(BigDecimal.valueOf(50.0));
        product.setCategory(Category.GROCERY);
        product.setMeasure(Measure.KILOGRAM);

        return product;
    }
}
