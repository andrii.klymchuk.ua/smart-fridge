package com.spdu.service;

import com.spdu.model.*;
import com.spdu.repository.EntityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class RecipeServiceImplTest {

    private static final int RECIPE_ID = 1;

    @Mock
    private EntityRepository<Recipe> recipeRepository;
    @Mock
    private  EntityService<RecipeIngredient> recipeIngredientService;

    private  EntityService<Recipe> recipeService;

    @BeforeEach
    void setUp() {
        recipeService = new RecipeServiceImpl(recipeRepository, recipeIngredientService);
    }

    @Test
    void getAllRecipesByIdWhenProductsExistShouldReturnActualResult() {
        List<Recipe> expectedResult = List.of(createRecipe(1, "test1"));

        when(recipeRepository.get(Set.of(RECIPE_ID))).thenReturn(List.of(createRecipe(1, "test1")));

        List<Recipe> actualResult = recipeService.get(Set.of(RECIPE_ID));

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void getAllRecipesWhenRecipesExistShouldReturnActualResult() {
        List<Recipe> expectedResult = List.of(createRecipe(1, "test1"), createRecipe(2, "test2"));

        when(recipeRepository.getAll()).thenReturn(List.of(createRecipe(1, "test1"), createRecipe(2, "test2")));

        List<Recipe> actualResult = recipeService.getAll();

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void getAllRecipesByIdWhenRecipesNotExistShouldReturnEmptyList() {
        List<Recipe> expectedResult = emptyList();

        when(recipeRepository.get(Set.of(RECIPE_ID))).thenReturn(emptyList());

        List<Recipe> actualResult = recipeService.get(Set.of(RECIPE_ID));

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void getAllRecipesWhenRecipesNotExistShouldReturnEmptyList() {
        List<Recipe> expectedResult = emptyList();

        when(recipeRepository.getAll()).thenReturn(emptyList());

        List<Recipe> actualResult = recipeService.getAll();

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void saveRecipeMustBeAssert() {
        EntityService<Recipe> service = mock(RecipeServiceImpl.class);
        doNothing().when(service).save(isA(Recipe.class));

        Recipe recipe = new Recipe();
        service.save(recipe);
        verify(service).save(recipe);

        assertAll(() -> recipeService.save(recipe));
        assertAll(() -> recipeService.save(createRecipe(1, "test1")));
    }

    @Test
    void deleteRecipeMustBeAssert() {
        EntityService<Recipe> service = mock(RecipeServiceImpl.class);

        doNothing().when(service).delete(isA(Recipe.class));
        Recipe recipe = new Recipe();
        service.delete(recipe);
        verify(service).delete(recipe);

        assertAll(() -> recipeService.delete(recipe));
        assertAll(() -> recipeService.delete(createRecipe(1, "test1")));
    }

    private Recipe createRecipe(Integer id, String name) {
        Recipe recipe = new Recipe();
        recipe.setId(id);
        recipe.setName(name);
        recipe.setRecipeIngredients(Collections.emptyList());

        return recipe;
    }
}
