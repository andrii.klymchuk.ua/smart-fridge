package com.spdu.service;

import com.spdu.model.RecipeIngredient;
import com.spdu.repository.EntityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Set;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class RecipeIngredientServiceImplTest {

    private static final int RECIPE_INGREDIENT_ID = 1;

    @Mock
    private EntityRepository<RecipeIngredient> recipeRepository;

    private  EntityService<RecipeIngredient> recipeService;

    @BeforeEach
    void setUp() {
        recipeService = new RecipeIngredientServiceImpl(recipeRepository);
    }

    @Test
    void getAllRecipeIngredientsByIdWhenProductsExistShouldReturnActualResult() {
        List<RecipeIngredient> expectedResult = List.of(createRecipeIngredient(1));

        when(recipeRepository.get(Set.of(RECIPE_INGREDIENT_ID))).thenReturn(List.of(createRecipeIngredient(1)));

        List<RecipeIngredient> actualResult = recipeService.get(Set.of(RECIPE_INGREDIENT_ID));

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void getAllRecipeIngredientsWhenProductsExistShouldReturnActualResult() {
        List<RecipeIngredient> expectedResult = List.of(createRecipeIngredient(1), createRecipeIngredient(2));

        when(recipeRepository.getAll()).thenReturn(List.of(createRecipeIngredient(1), createRecipeIngredient(2)));

        List<RecipeIngredient> actualResult = recipeService.getAll();

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void getAllRecipeIngredientsByIdWhenProductsNotExistShouldReturnEmptyList() {
        List<RecipeIngredient> expectedResult = emptyList();

        when(recipeRepository.get(Set.of(RECIPE_INGREDIENT_ID))).thenReturn(emptyList());

        List<RecipeIngredient> actualResult = recipeService.get(Set.of(RECIPE_INGREDIENT_ID));

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void getAllRecipeIngredientsWhenProductsNotExistShouldReturnEmptyList() {
        List<RecipeIngredient> expectedResult = emptyList();

        when(recipeRepository.getAll()).thenReturn(emptyList());

        List<RecipeIngredient> actualResult = recipeService.getAll();

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void saveRecipeIngredientMustBeAssert() {
        EntityService<RecipeIngredient> service = mock(RecipeIngredientServiceImpl.class);
        doNothing().when(service).save(isA(RecipeIngredient.class));

        RecipeIngredient ingredient = new RecipeIngredient();
        service.save(ingredient);
        verify(service, times(1)).save(ingredient);

        assertAll(() -> recipeService.save(ingredient));
        assertAll(() -> recipeService.save(createRecipeIngredient(1)));
    }

    @Test
    void deleteRecipeIngredientMustBeAssert() {
        EntityService<RecipeIngredient> service = mock(RecipeIngredientServiceImpl.class);
        doNothing().when(service).delete(isA(RecipeIngredient.class));

        RecipeIngredient ingredient = new RecipeIngredient();
        service.delete(ingredient);
        verify(service, times(1)).delete(ingredient);

        assertAll(() -> recipeService.delete(ingredient));
        assertAll(() -> recipeService.delete(createRecipeIngredient(1)));
    }

    private RecipeIngredient createRecipeIngredient(int productId) {
        RecipeIngredient ingredient = new RecipeIngredient();
        ingredient.setRecipeId(1);
        ingredient.setProductId(productId);
        ingredient.setAmount(50);

        return ingredient;
    }
}
