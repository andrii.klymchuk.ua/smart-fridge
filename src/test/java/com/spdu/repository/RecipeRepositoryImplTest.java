package com.spdu.repository;

import com.spdu.mapper.RecipeMapper;
import com.spdu.model.Recipe;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.*;

@JdbcTest
@ActiveProfiles("test")
public class RecipeRepositoryImplTest {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private EntityRepository<Recipe> recipeRepository;

    @BeforeEach
    void setUp() {
        RecipeMapper recipeMapper = new RecipeMapper();
        recipeRepository = new RecipeRepositoryImpl(namedParameterJdbcTemplate, recipeMapper);
    }

    @Test
    @Sql("recipe-data.sql")
    void getByIdsWhenRecipesExistShouldReturnActualResult() {
        List<Recipe> expectedResult = List.of(createRecipe(1, "test-1"), createRecipe(2, "test-2"));
        List<Recipe> actualResult = recipeRepository.get(List.of(1, 2));

        assertEquals(expectedResult, actualResult);
    }

    @Test
    @Sql("recipe-data.sql")
    void getAllWhenRecipesExistShouldReturnActualResult() {
        List<Recipe> expectedResult = List.of(createRecipe(1, "test-1"),createRecipe(2, "test-2"));
        List<Recipe> actualResult = recipeRepository.getAll();

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void getByIdsWhenRecipesNotExistShouldReturnEmptyList() {
        List<Recipe> expectedResult = emptyList();
        List<Recipe> actualResult = recipeRepository.get(List.of(1, 2));

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void getAllWhenRecipesNotExistShouldReturnEmptyList() {
        List<Recipe> expectedResult = emptyList();
        List<Recipe> actualResult = recipeRepository.getAll();

        assertEquals(expectedResult, actualResult);
    }

    @Test
    @Sql("recipe-data.sql")
    void getAllWhenDeleteRecipesReturnActualResult() {
        List<Recipe> expectedResult = List.of(createRecipe(2, "test-2"));
        recipeRepository.delete(createRecipe(1, "test-1"));
        List<Recipe> actualResult = recipeRepository.getAll();

        assertEquals(expectedResult, actualResult);

        recipeRepository.delete(createRecipe(3, "test-3"));
        actualResult = recipeRepository.getAll();

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void getAllWhenDeleteRecipeReturnEmptyList() {
        List<Recipe> expectedResult = emptyList();
        recipeRepository.delete(createRecipe(1, "test-1"));
        List<Recipe> actualResult = recipeRepository.getAll();

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void getAllWhenDeleteNullReturnNullPointerException() {
        assertThrows(NullPointerException.class, () -> recipeRepository.delete(null));
    }

    @Test
    void getAllWhenSaveRecipeReturnActualResult() {
        Recipe newRecipe = createRecipe(null, "test");
        recipeRepository.save(newRecipe);

        assertNotNull(newRecipe.getId());

        List<Recipe> expectedResult = List.of(newRecipe);
        List<Recipe> actualResult = recipeRepository.getAll();

        assertEquals(expectedResult, actualResult);

        newRecipe.setName("test-1");
        recipeRepository.save(newRecipe);
        expectedResult = List.of(newRecipe);
        actualResult = recipeRepository.getAll();

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void getAllWhenSaveNullReturnNullPointerException() {
        assertThrows(NullPointerException.class, () -> recipeRepository.save(null));
    }

    private Recipe createRecipe(Integer id, String name) {
        Recipe recipe = new Recipe();
        recipe.setId(id);
        recipe.setName(name);

        return recipe;
    }
}
