package com.spdu.repository;

import com.spdu.mapper.RecipeIngredientMapper;
import com.spdu.model.RecipeIngredient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

@JdbcTest
@ActiveProfiles("test")
public class RecipeIngredientRepositoryImplTest {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private EntityRepository<RecipeIngredient> ingredientRepository;

    @BeforeEach
    void setUp() {
        RecipeIngredientMapper ingredientMapper = new RecipeIngredientMapper();
        ingredientRepository = new RecipeIngredientRepositoryImpl(namedParameterJdbcTemplate, ingredientMapper);
    }

    @Test
    @Sql("recipe_ingredient-data.sql")
    void getByIdsWhenRecipeIngredientsExistShouldReturnActualResult() {
        List<RecipeIngredient> expectedResult = List.of(
            createRecipeIngredient(1),
            createRecipeIngredient(2)
        );
        List<RecipeIngredient> actualResult = ingredientRepository.get(List.of(1, 2));

        assertEquals(expectedResult, actualResult);
    }

    @Test
    @Sql("recipe_ingredient-data.sql")
    void getAllWhenRecipeIngredientsExistShouldReturnActualResult() {
        List<RecipeIngredient> expectedResult = List.of(
                createRecipeIngredient(1),
                createRecipeIngredient(2)
        );
        List<RecipeIngredient> actualResult = ingredientRepository.getAll();

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void getByIdsWhenRecipeIngredientsNotExistShouldReturnEmptyList() {
        List<RecipeIngredient> expectedResult = emptyList();
        List<RecipeIngredient> actualResult = ingredientRepository.get(List.of(1, 2));

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void getAllWhenRecipeIngredientsNotExistShouldReturnEmptyList() {
        List<RecipeIngredient> expectedResult = emptyList();
        List<RecipeIngredient> actualResult = ingredientRepository.getAll();

        assertEquals(expectedResult, actualResult);
    }

    @Test
    @Sql("recipe_ingredient-data.sql")
    void getAllWhenDeleteRecipeIngredientsReturnActualResult() {
        List<RecipeIngredient> expectedResult = List.of(createRecipeIngredient(2));
        ingredientRepository.delete(createRecipeIngredient(1));
        List<RecipeIngredient> actualResult = ingredientRepository.getAll();

        assertEquals(expectedResult, actualResult);

        ingredientRepository.delete(createRecipeIngredient(3));
        actualResult = ingredientRepository.getAll();

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void getAllWhenDeleteRecipeIngredientReturnEmptyList() {
        List<RecipeIngredient> expectedResult = emptyList();
        ingredientRepository.delete(createRecipeIngredient(1));
        List<RecipeIngredient> actualResult = ingredientRepository.getAll();

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void getAllWhenDeleteNullReturnNullPointerException() {
        assertThrows(NullPointerException.class, () -> ingredientRepository.delete(null));
    }

    @Test
    @Sql("recipe_ingredient-data.sql")
    void getAllWhenSaveRecipeIngredientReturnActualResult() {
        RecipeIngredient ingredient = createRecipeIngredient(3);
        ingredientRepository.save(ingredient);
        List<RecipeIngredient> expectedResult = List.of(ingredient);
        List<RecipeIngredient> actualResult = ingredientRepository.getAll();

        assertTrue(actualResult.containsAll(expectedResult));

        ingredient.setAmount(100);
        ingredientRepository.save(ingredient);
        expectedResult = List.of(ingredient);
        actualResult = ingredientRepository.getAll();

        assertTrue(actualResult.containsAll(expectedResult));
    }

    @Test
    void getAllWhenSaveNullReturnNullPointerException() {
        assertThrows(NullPointerException.class, () -> ingredientRepository.save(null));
    }

    private RecipeIngredient createRecipeIngredient(Integer productId) {
        RecipeIngredient ingredient = new RecipeIngredient();
        ingredient.setRecipeId(1);
        ingredient.setProductId(productId);
        ingredient.setAmount(0.5);

        return ingredient;
    }
}
