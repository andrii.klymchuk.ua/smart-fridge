INSERT INTO product (id, name, measure, category, price) VALUES (1, 'test-product-1', 'KILOGRAM', 'GROCERY', 50);
INSERT INTO product (id, name, measure, category, price) VALUES (2, 'test-product-2', 'KILOGRAM', 'FRUITS',  50);
INSERT INTO product (id, name, measure, category, price) VALUES (3, 'test-product-3', 'KILOGRAM', 'FRUITS',  50);

INSERT INTO recipe (id, name) VALUES (1, 'test-recipe-1');

INSERT INTO recipe_ingredient (recipe_id, product_id, amount) VALUES (1, 1, 0.5);
INSERT INTO recipe_ingredient (recipe_id, product_id, amount) VALUES (1, 2, 0.5);