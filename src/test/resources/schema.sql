CREATE TABLE IF NOT EXISTS product
(
    id       SERIAL       NOT NULL,
    name     VARCHAR(255) NOT NULL UNIQUE,
    measure  VARCHAR(255) NOT NULL,
    category VARCHAR(255) NOT NULL,
    price    NUMERIC      NOT NULL,

    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS recipe
(
    id   SERIAL       NOT NULL,
    name VARCHAR(255) NOT NULL,

    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS recipe_ingredient
(
    recipe_id  INTEGER          NOT NULL REFERENCES recipe  (id) ON DELETE CASCADE,
    product_id INTEGER          NOT NULL REFERENCES product (id) ON DELETE CASCADE,
    amount     DOUBLE PRECISION NOT NULL,

    PRIMARY KEY (recipe_id, product_id)
);

CREATE TABLE IF NOT EXISTS fridge_product
(
    product_id INTEGER          NOT NULL REFERENCES product (id) ON DELETE CASCADE,
    amount     DOUBLE PRECISION NOT NULL,

    PRIMARY KEY (product_id)
);

CREATE TABLE IF NOT EXISTS users
(
    id       SERIAL       NOT NULL,
    username VARCHAR(100) NOT NULL UNIQUE,
    password VARCHAR(100) NOT NULL,
    enabled  boolean      NOT NULL DEFAULT TRUE,
    role     VARCHAR(10)  NOT NULL DEFAULT 'USER',
    PRIMARY KEY (id)
);
