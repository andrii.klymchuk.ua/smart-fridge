FROM openjdk:11

ARG JAR_FILE=build/libs/spdu-0.0.1-SNAPSHOT.jar

COPY ${JAR_FILE} app.jar
#
#EXPOSE 8080

ENTRYPOINT ["java","-jar","/app.jar"]


#FROM tomcat:9-jdk8
#ADD target/MyWebApp-1.0.war /usr/local/tomcat/webapps/MyWebApp-1.0.war
#EXPOSE 8080:8080
#CMD ["catalina.sh", "run"]
#
#DockerCompose.yml
#
#version: '3.1'
#services:
#  app:
#    container_name: springboot-postgresql
#    image: springboot-app
#    build: ./
#    ports:
#      - "8080:8080"
#    depends_on:
#      - postgresqldb
#  postgresqldb:
#    image: postgres
#    build:
#      context: ./
#      dockerfile: Dockerfile.postgresql
#    ports:
#      - "5454:5454"
#    environment:
##      - PGDATA=/var/lib/postgresql/data
##      - POSTGRES_PASSWORD=*********YOUR PASSWORD
##      - POSTGRES_USER=postgres
#      - POSTGRES_PASSWORD=root
#      - POSTGRES_USER=postgres
#      - POSTGRES_DATABASE=smart-fridge
##      - DATASOURCE_URL=jdbc:postgresql://localhost:5432/smart-fridge
#
#

#version: '3'
#services:
#  my-web-app-db:
#    image: postgres
#    container_name: springbootapp
#    environment:
##      - SPRING_DATASOURCE_URL=jdbc:postgresql://localhost:5432/smart-fridge
#      - POSTGRES_USER=postgres
#      - POSTGRES_PASSWORD=root
#      - POSTGRES_DATABASE=smart-fridge
#  my-web-app:
#    image: my-web-app
#    build: .
#    ports:
#      - "8080:8080"
#    depends_on:
#      - my-web-app-db


#version: '3'
#services:
#  my-web-app-db:
#    image: postgres
#    container_name: springbootapp
#    environment:
#      #      - SPRING_DATASOURCE_URL=jdbc:postgresql://localhost:5432/smart-fridge
#      - POSTGRES_USER=postgres
#      - POSTGRES_PASSWORD=root
#      - POSTGRES_DATABASE=smart-fridge
#  my-web-app:
#    build: .
#    ports:
#      - "8080:8080"
#    depends_on:
#      - my-web-app-db
